const express = require('express');

const router = express.Router();
const { getUserProfile, deleteUser, editPasswordUser } = require('../controllers/userController');

router.get('/', getUserProfile);
router.patch('/password', editPasswordUser);
router.delete('/', deleteUser);

module.exports = {
  userRouter: router,
};
