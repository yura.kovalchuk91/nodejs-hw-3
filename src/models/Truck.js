const mongoose = require('mongoose');
const Joi = require('joi');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRIGHT', 'LARGE STRIGHT'],
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },

});

module.exports = {
  Truck,
  // userJoiSchema
};
