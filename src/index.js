const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

require('dotenv').config();

mongoose.connect('mongodb+srv://Yurii:iPnbaYqbRpdXPNgC@yuriikovalchukstudy.hfiyktc.mongodb.net/?retryWrites=true&w=majority');

const { authRouter } = require('./routers/authRouter');
const { userRouter } = require('./routers/userRouter');
const { truckRouter } = require('./routers/truckRouter');
const { loadRouter } = require('./routers/loadRouter');

const { authMiddleware } = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', authMiddleware, userRouter);
app.use('/api/trucks', authMiddleware, truckRouter);
app.use('/api/loads', authMiddleware, loadRouter);

const start = async () => {
  app.listen(8080, () => console.log('Server started listening on pc'));
};

start();

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'My Server error' });
}
