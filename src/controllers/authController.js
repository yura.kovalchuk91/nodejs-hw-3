const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User, userJoiSchema } = require('../models/User');

const registerUser = async (req, res, next) => {
  const { email, password, role } = req.body;

  if (!email || !password || !role) {
    res.status(400).json({ message: 'U need to fill all fields' });
  }
  await userJoiSchema.validateAsync({ email, password, role });

  const salt = await bcryptjs.genSalt(10);
  const user = new User({
    email,
    password: await bcryptjs.hash(password, salt),
    role,
  });
  user.save()
    .then((saved) => res.status(200).json({
      message: 'success',
      saved,
    }))
    .catch((err) => {
      next(err);
    });
};

const loginUser = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });

  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = {
      email: user.email,
      role: user.role,
      userId: user._id,
      createdDate: user.createdDate,
    };
    const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
    return res.status(200).json({ jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser
};
